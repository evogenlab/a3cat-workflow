import logging
import os
import datetime
from utils import setup_logging


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    # Retrieve info from snakemake
    logging.info('Getting data from Snakemake')
    ncbi_query_flag_file = snakemake.input.ncbi_query_flag_file[0]
    summary_file_path = snakemake.input.summary_file[0]
    runs_dir = snakemake.params.runs_dir
    output_file_path = snakemake.output[0]

    logging.info('Get query time from flag file')
    # Get modification time of flag file for NCBI query and convert to readable time
    query_time = datetime.datetime.fromtimestamp(os.path.getmtime(ncbi_query_flag_file))
    query_time_str = query_time.strftime('%Y-%m-%d %H:%M:%S')

    logging.info('Load assemblies for current run')
    with open(summary_file_path) as summary_file:
        summary_file.readline()
        current_assemblies = tuple(line.split('\t')[0] for line in summary_file if len(line) > 5)

    logging.info('Find last run')
    try:
        previous_runs = [datetime.datetime.strptime(run, '%Y-%m-%d_%H:%M:%S') for run in os.listdir(runs_dir)]
        last_run = max(previous_runs).strftime('%Y-%m-%d_%H:%M:%S')
        last_summary_file_path = os.path.join(runs_dir, last_run, 'summary.tsv')
        logging.info('Load assemblies for last run')
        with open(last_summary_file_path) as last_summary_file:
            last_summary_file.readline()
            last_run_assemblies = set(line.split('\t')[0] for line in last_summary_file if len(line) > 5)
    except FileNotFoundError:
        last_run_assemblies = set()

    logging.info('Generate info file')
    with open(output_file_path, 'w') as output_file:
        output_file.write(f'Query date\t{query_time_str}\n')
        output_file.write(f'Number of assemblies\t{len(current_assemblies)}\n')
        output_file.write(f'New assemblies:\n')
        for assembly in current_assemblies:
            if assembly not in last_run_assemblies:
                output_file.write(f'\t- {assembly}\n')
