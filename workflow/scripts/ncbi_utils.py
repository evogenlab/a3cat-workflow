import logging
import re
import requests
import time
from urllib.error import HTTPError
from Bio import Entrez


def query_wrapper(tool, max_tries, sleep_between_tries, **args):
    '''
    Small wrapper function to query NCBI.
    Handles all Entrez tools, arguments are gotten from **args.
    If the query fails, will attempt again every <sleep_between_tries> seconds
    until <max_tries>.
    '''
    retry_count = 0
    while True:
        logging.info(f'Querying <{tool}>: attempt {retry_count + 1}/{max_tries}')
        try:
            handle = getattr(Entrez, tool)(**args)
            record = Entrez.read(handle, validate=False)
            logging.info(f'Querying <{tool}> was successful on attempt {retry_count + 1}/{max_tries}')
            break
        except (HTTPError, RuntimeError) as error:
            logging.error(f'Querying <{tool}> failed on attempt {retry_count + 1}/{max_tries}: <{error}>')
            if 'Empty result' in str(error):
                logging.error(f'No assembly found for this query.')
                exit(1)
            retry_count += 1
            if retry_count == max_tries:
                logging.error(f'Reached max attempts, cannot connect to NCBI.')
                exit(1)
        time.sleep(sleep_between_tries)
    return record


def get_article_from_bioproject(bioproject_id):
    '''
    Attempt to retrieve a publication title and Pubmed ID from a Bioproject ID.
    There seems to be a problem with Biopython's efetch implementation, so we had to query
    NCBI eutils directly with requests to get bioproject data.
    '''
    article_string, pubmed_id = '', ''
    if len(bioproject_id) < 2:
        return article_string, pubmed_id
    regex = r'<Publication id="(\d+)"'
    url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=bioproject&id={bioproject_id}'
    # Query NCBI directly with requests to retrieve Publication ID from Bioproject data
    response = requests.get(url)
    parsing = re.search(regex, response.text)
    # If an ID was found, query pubmed to recover the article's title and year
    if parsing is not None:
        pubmed_id = parsing.group(1)
        record = query_wrapper('efetch', 3, 1, db='pubmed', id=pubmed_id, retmode='xml')
        title, year = '', ''
        try:
            article_data = record['PubmedArticle'][0]['MedlineCitation']['Article']
            title = article_data['ArticleTitle']
            if 'Year' in article_data['ArticleDate'][0]:
                year = article_data['ArticleDate'][0]['Year']
            elif 'Year' in article_data['JournalIssue']['PubDate']:
                year = article_data['JournalIssue']['PubDate']['Year']
        except KeyError:
            pass
        except IndexError:
            pass
        # Export format: <title> (<year>)
        article_string = f'{title} ({year})'
    return article_string, pubmed_id
