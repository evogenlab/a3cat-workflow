import logging
import pandas
from utils import setup_logging, load_json_to_dict

BUSCO_FIELDS_ORDER = ['complete', 'single', 'duplicated', 'fragmented', 'missing']


def parse_json_record(data):
    '''
    Parse a json record for an assembly from the summary file to generate
    a panda data frame row.
    '''
    # Extract nested bioproject information and only retained the accession column
    bioproject = pandas.json_normalize(data,
                                       record_path=['assemblyInfo',
                                                    'bioprojectLineage',
                                                    'bioprojects'])
    # Extract only "accession" column
    bioproject = bioproject[['accession']]
    # Concatenate all bioprojects into a string "accession1,accession2..."
    bioproject.columns = ['assemblyInfo.bioproject']
    bioprojects = ','.join(bioproject['assemblyInfo.bioproject'])
    # Create a mini data frame with bioprojects
    bioproject = pandas.DataFrame(data={'assemblyInfo.bioproject': [bioprojects]})
    # Parse all information
    table_data = pandas.json_normalize(data)
    # Concatenate info with bioprojects
    table_data = pandas.concat([bioproject, table_data], axis=1)
    return table_data


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from Snakemake')
    summary_file_path = snakemake.input[0]
    summary_table_file_path = snakemake.output[0]
    summary_fields_file_path = snakemake.params.summary_fields_file

    logging.info('Loading data from JSON summary')
    # Parse json summary into a list of panda data frames
    json_data = load_json_to_dict(summary_file_path)
    summary = []
    for assembly, data in json_data.items():
        record = parse_json_record(data)
        summary.append(record)
    logging.info(f'Loaded data for {len(summary)} assemblies')

    # Concatenate data frames for all assemblies
    summary_table = pandas.concat(summary)

    logging.info('Formatting table headers')
    # Load fields specified in JSON config file and add BUSCO fields
    json_fields = load_json_to_dict(summary_fields_file_path)
    busco_headers = (header for header in summary_table.columns if 'busco' in header)
    lineages = {h.split('.')[1] for h in busco_headers}
    for lineage in lineages:
        for field in BUSCO_FIELDS_ORDER:
            header = f'busco.{lineage}.{field}'
            # Format busco headers as "<Lineage> <Type> BUSCO"
            header_string = f'{lineage.capitalize()} {field.capitalize()} BUSCO'
            json_fields[header] = header_string

    logging.info('Filtering table')
    # Filter final data frame to only retain fields specified in JSON config file
    summary_table = summary_table.drop([c for c in summary_table.columns if c not in json_fields], axis=1)
    # Rename columns according to labels defined in JSON config file
    summary_table.columns = [json_fields[c] for c in summary_table.columns]
    # Drop fields missing from JSON output
    json_fields = {k: v for k, v in json_fields.items() if v in summary_table.columns}
    # Reorder columns to follow order from JSON config file
    summary_table = summary_table[list(json_fields.values())]
    logging.info(f'Saving summary table to <{summary_table_file_path}>')
    summary_table.to_csv(summary_table_file_path, sep='\t', index=False)
