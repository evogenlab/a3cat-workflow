import jinja2
import logging
from collections import defaultdict
from utils import setup_logging, load_json_to_dict


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    table_file_path = snakemake.input.summary
    html_file_path = snakemake.output[0]
    cfg_file_path = snakemake.params.table_cfg_file
    template_file_path = snakemake.params.table_template_file

    # Properties file path
    logging.info(f'Loading table properties from {cfg_file_path}')
    properties = load_json_to_dict(cfg_file_path)

    logging.info('Loading template with jinja')
    # Load template with jinja
    with open(template_file_path) as template_file:
        template = jinja2.Template(template_file.read())

    logging.info('Getting information from summary table')
    # Loading data
    data = []
    with open(table_file_path) as table_file:
        header = table_file.readline()[:-1].split('\t')
        for i, line in enumerate(table_file):
            if len(line) < 2:
                continue
            fields = line[:-1].split('\t')
            if len(fields) != len(header):
                logging.warning(f'Incorrect number of fields <{len(fields)}> for line {i + 2} (expected {len(header)}), skipped line.')
                continue
            data.append(fields)

    # Assign data type in column body (controls data formatting) and default
    # column visibility
    col_type = defaultdict(list)
    hidden_columns = []
    for column, info in properties.items():
        if column not in header:
            continue
        if 'type' in info:
            col_type[info['type']].append(header.index(column))
        if 'visible' in info and not info['visible']:
            hidden_columns.append(header.index(column))
    for i, h in enumerate(header):
        if 'BUSCO' in h:
            col_type['float'].append(i)

    # Assign column labels
    labels = [properties[h]['label'] if h in properties else
              h.replace(' BUSCO', '') for h in header]

    logging.info('Rendering template')
    html_page = template.render(table_header=labels,
                                table_rows=data,
                                num_col=col_type['num'],
                                float_col=col_type['float'],
                                string_col=col_type['string'],
                                hidden_col=hidden_columns)
    with open(html_file_path, 'w') as f:
        f.write(html_page)

    logging.info('Succesfully generated HTML output')
