import logging
import os
import re
import requests
from utils import setup_logging, create_dir, touch, load_json_to_dict


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from Snakemake')
    # Busco dataset base url
    assembly_info_file_path = snakemake.input.info
    taxonomy_file_path = snakemake.input.taxonomy[0]
    blacklist_file_path = snakemake.input.blacklist
    dataset_base_url = snakemake.params.dataset_base_url
    dataset_base_url = dataset_base_url.rstrip('/')
    output_dir = snakemake.output[0]
    min_assembly_size = int(snakemake.params.min_assembly_size)
    min_assembly_n50 = int(snakemake.params.min_assembly_n50)
    accession = snakemake.wildcards.accession

    create_dir(output_dir)

    # Blacklist is a tabulated file where each line contains an assembly accession and
    # a lineage dataset to blacklist, e.g. "GCA_012354.1    arthropdoa"
    logging.info('Loading blacklist')
    with open(blacklist_file_path) as blacklist_file:
        blacklist = {line.rstrip('\n').split('\t')[1] for line in blacklist_file if line.split('\t')[0] == accession}

    logging.info('Checking assembly size and N50')
    assembly_size = int(load_json_to_dict(assembly_info_file_path)['assemblyStats']['totalUngappedLength'])
    assembly_n50 = int(load_json_to_dict(assembly_info_file_path)['assemblyStats']['scaffoldN50'])
    if assembly_size < min_assembly_size:
        logging.info(f'Assembly is smaller than minimum required size ({assembly_size} < {min_assembly_size}), skipping BUSCO')
        exit(0)
    if assembly_n50 < min_assembly_n50:
        logging.info(f'Assembly N50 is lower than minimum required N50 ({assembly_n50} < {min_assembly_n50}), skipping BUSCO')
        exit(0)

    logging.info('Retrieving taxonomy info')
    taxonomy = load_json_to_dict(taxonomy_file_path)
    taxa_list = [taxon.lower() for taxon in taxonomy.values()]

    # Available BUSCO lineages are accessible at URL defined in the config file
    # For all Arthropod lineages, as well as metazoa and eukaryota, BUSCO lineage names match
    # NCBI taxonomy names. Therefore, we can check if a taxon from a species' lineage is available
    # as a BUSCO lineage by checking the response code when querying the URL.
    logging.info(f'Retrieving available BUSCO lineages for <{accession}> from BUSCO''s website')
    response = requests.get(dataset_base_url)
    if response.status_code != 200:
        logging.error(f'Invalid return code {response.status_code} when retrieving available BUSCO lineages.')
        exit(1)
    content = response.text
    available_lineages = set(re.findall(r'<a href="([a-zA-Z]+)_odb10', content))
    if len(available_lineages) == 0:
        logging.error(f'Error parsing response from BUSCO''s website: could not find available lineages')
        exit(1)
    busco_lineages = [taxon for taxon in taxa_list if taxon in available_lineages]
    logging.info(f'BUSCO lineages for <{accession}>:')
    for lineage in busco_lineages:
        if lineage in blacklist:
            logging.info(f'    - {lineage} was blacklisted, skipping file creation')
            continue
        logging.info(f'    - {lineage}')
        # Create a file named <busco_lineage>.txt in a directory for each available lineage for this assembly.
        # These files are used as input to launch busco runs later.
        run_file_path = os.path.join(output_dir, f'{lineage}.txt')
        touch(run_file_path, update_timestamp=False)

    logging.info(f'Successfully created run files for all lineages for <{accession}>')
