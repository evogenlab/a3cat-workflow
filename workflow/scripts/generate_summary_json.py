import logging
import os
import re
from utils import setup_logging, load_json_to_dict, save_dict_to_json
from collections import defaultdict

# BUSCO results
BUSCO_RESULTS = ('complete', 'single', 'duplicated', 'fragmented', 'missing')

# Define regex used to parse BUSCO summary files
SUMMARY_REGEX = r'C:-?(\d+.\d)%\[S:-?(\d+.\d)%,D:-?(\d+.\d)%\],F:-?(\d+.\d)%,M:-?(\d+.\d)%,n:\d+'


def process_info_file(info_file_path, summary):
    '''
    '''
    # Get accession number from file name
    accession = os.path.splitext(os.path.split(info_file_path)[-1])[0]
    logging.info(f'Getting assembly metadata for assembly <{accession}>')
    summary[accession] = load_json_to_dict(info_file_path)


def process_busco_file(busco_file_path, summary):
    '''
    '''
    # Get accession number from file path
    busco_dir_path = os.path.dirname(busco_file_path)
    accession = os.path.split(busco_dir_path)[-1]
    # Get list of lineage folders in busco directory
    lineages = [d for d in os.listdir(busco_dir_path) if
                os.path.isdir(os.path.join(busco_dir_path, d))]
    logging.info(f'Found {len(lineages)} lineages for <{accession}> ({",".join(lineages)})')
    summary[accession]['busco'] = defaultdict(dict)
    for lineage in lineages:
        busco_file_path = os.path.join(busco_dir_path, lineage, 'short_summary.txt')
        if not os.path.isfile(busco_file_path):
            logging.error(f'Could not find busco scores file <{busco_file_path}>')
            continue
        logging.info(f'Getting busco results for assembly <{accession}> and lineage <{lineage}>')
        with open(busco_file_path) as busco_file:
            found_results_line = False
            # Iterate over BUSCO summary files using a regex to parse BUSCO results
            for line in busco_file:
                matches = re.search(SUMMARY_REGEX, line)
                if matches:
                    found_results_line = True
                    summary[accession]['busco'][lineage][BUSCO_RESULTS[0]] = float(matches.group(1))
                    summary[accession]['busco'][lineage][BUSCO_RESULTS[1]] = float(matches.group(2))
                    summary[accession]['busco'][lineage][BUSCO_RESULTS[2]] = float(matches.group(3))
                    summary[accession]['busco'][lineage][BUSCO_RESULTS[3]] = float(matches.group(4))
                    summary[accession]['busco'][lineage][BUSCO_RESULTS[4]] = float(matches.group(5))
            if not found_results_line:
                logging.error(f'Could not find busco scores line in file <{busco_file_path}>')


def process_taxonomy_file(taxonomy_file_path, summary):
    '''
    '''
    # Get accession number from file name
    accession = os.path.splitext(os.path.split(taxonomy_file_path)[-1])[0]
    logging.info(f'Getting taxonomy information for assembly <{accession}>')
    summary[accession]['taxonomy'] = load_json_to_dict(taxonomy_file_path)


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Getting data from Snakemake')
    busco_files = snakemake.input.busco
    info_files = snakemake.input.info
    taxonomy_files = snakemake.input.taxonomy
    summary_file_path = snakemake.output[0]

    summary = dict()

    # Parse info files into the summary dictionary
    for info_file_path in info_files:
        process_info_file(info_file_path, summary)

    # Parse BUSCO results into the summary dictionary
    for busco_file_path in busco_files:
        process_busco_file(busco_file_path, summary)

    # Parse taxonomy info files into the summary dictionary
    for taxonomy_file_path in taxonomy_files:
        process_taxonomy_file(taxonomy_file_path, summary)

    logging.info(f'Saving summary to <{summary_file_path}>')
    save_dict_to_json(summary_file_path, summary)
