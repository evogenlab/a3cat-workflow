import logging
import os
import shutil
import subprocess
import json
from utils import setup_logging, create_dir, touch

RESULTS_DIR = 'results'


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    # Retrieve info from snakemake
    logging.info('Getting data from Snakemake')
    assembly_info_dir_path = os.path.dirname(snakemake.output[0])
    base_taxon = snakemake.params.base_taxon

    create_dir(assembly_info_dir_path)

    logging.info('Getting list of accession numbers from NCBI')
    # Query NCBI datasets CLI to retrieve accession numbers for all assemblies in base taxon
    accessions = []
    offset = 0
    page_size = 1000
    while True:
        cmd = [
            'datasets', 'summary', 'genome', 'taxon', base_taxon,
            '--assembly-source', 'refseq,genbank',
            '--as-json-lines',
            '--page-size', str(page_size),
            '--offset', str(offset)
        ]
        
        result = subprocess.run(cmd, capture_output=True, text=True)
        if result.returncode != 0:
            raise RuntimeError(f"datasets command failed: {result.stderr}")
        
        # Parse the JSON lines output
        assemblies = [json.loads(line) for line in result.stdout.splitlines() if line.strip()]
        if not assemblies:
            break
            
        # Extract GCA accessions
        accessions += [assembly['assemblyInfo']['assemblyAccession'] 
                      for assembly in assemblies 
                      if assembly['assemblyInfo']['assemblyAccession'].startswith('GCA_')]
        
        offset += page_size
        
    logging.info(f'Retrieved {len(accessions)} assemblies')

    # Check for assemblies with updated minor version
    processed_assemblies = {f.split('.')[0]: f.rstrip('.txt') for f in os.listdir(assembly_info_dir_path)}
    updated_assemblies = [f.split('.')[0] for f in accessions if f not in processed_assemblies.values() and f.split('.')[0] in processed_assemblies]
    # Remove files for updated assemblies
    results_subdirs = [d for d in os.listdir(RESULTS_DIR) if os.path.isdir(os.path.join(RESULTS_DIR, d))]
    for subdir in results_subdirs:
        subdir_path = os.path.join(RESULTS_DIR, subdir)
        matching_content = []
        for updated_assembly in updated_assemblies:
            matching_content += [c for c in os.listdir(subdir_path) if updated_assembly in c]
        print(matching_content)
        for content in matching_content:
            content_path = os.path.join(subdir_path, content)
            if os.path.isfile(content_path):
                os.remove(content_path)
            elif os.path.isdir(content_path):
                shutil.rmtree(content_path)

    logging.info('Initializing json files for new assemblies')
    new_assemblies = 0
    for accession in accessions:
        # If file already exists, timestamp is not updated --> no downstream
        # execution in Snakemake
        new_assemblies += touch(os.path.join(assembly_info_dir_path, f'{accession}.txt'), update_timestamp=False)
    logging.info(f'Successfully initialized json files for {new_assemblies} new assemblies')
