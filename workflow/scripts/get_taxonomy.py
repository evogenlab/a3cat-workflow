import logging
import os
from ete3 import NCBITaxa
from utils import setup_logging, create_dir, load_json_to_dict, save_dict_to_json, check_ete3_db

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    info_file_path = snakemake.input.info_file
    db_file_path = snakemake.params.ete3_db
    taxonomy_file_path = snakemake.output[0]
    accession = snakemake.wildcards.accession

    create_dir(os.path.dirname(taxonomy_file_path))

    logging.info('Loading ete3 database')
    # check_ete3_db(db_file_path)
    logging.info(db_file_path)
    ncbi = NCBITaxa(dbfile=db_file_path)

    logging.info(f'Retrieving taxonomy information for assembly <{accession}>')
    try:
        taxid = load_json_to_dict(info_file_path)['taxId']
    except KeyError as e:
        logging.error(f'Could not retrieve taxId from file {info_file_path} (json field: {e})')

    lineage = ncbi.get_lineage(taxid)
    lineage_names = ncbi.get_taxid_translator(lineage)
    lineage_ranks = ncbi.get_rank(lineage)
    taxonomy = {rank: lineage_names[taxid] for taxid, rank in lineage_ranks.items()}

    logging.info(f'Saving taxonomy info to <{taxonomy_file_path}>')
    save_dict_to_json(taxonomy_file_path, taxonomy)

    logging.info(f'Successfully retrieved taxonomy information for assembly <{accession}>')
