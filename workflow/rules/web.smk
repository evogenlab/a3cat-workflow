

rule setup_web_folder:
    '''
    '''
    input:
        css = config['web']['css_dir']
    output:
        css_dir = directory('results/web/css'),
        img_dir = directory('results/web/img')
    benchmark:
        'benchmarks/summary/setup_web_folder.tsv'
    log:
        'logs/summary/setup_web_folder.txt'
    conda:
        '../envs/summary.yaml'
    threads: get_threads('setup_web_folder')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('setup_web_folder', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('setup_web_folder', attempt)
    shell:
        'mkdir -p {output.img_dir} 2>&1 > {log};'
        'cp -rf {input.css} {output.css_dir} 2>&1 > {log};'


rule generate_summary_html:
    '''
    '''
    input:
        summary = rules.generate_summary_table.output[0],
        web_folder = rules.setup_web_folder.output
    output:
        'results/web/table.html'
    benchmark:
        'benchmarks/summary/generate_summary_html.tsv'
    log:
        'logs/summary/generate_summary_html.txt'
    conda:
        '../envs/summary.yaml'
    threads: get_threads('generate_summary_html')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('generate_summary_html', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('generate_summary_html', attempt)
    params:
        table_cfg_file = config['web']['table_cfg'],
        table_template_file = os.path.join(config['web']['templates_dir'], 'table.html')
    script:
        '../scripts/generate_summary_html.py'
