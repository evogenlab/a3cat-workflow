
def summary_input(wildcards):
    '''
    Aggregate input for all info file, taxonomy files, and busco runs.
    '''
    all_input = {'busco': [], 'info': [], 'taxonomy': []}
    # First get all accession numbers
    checkpoints.get_accession_numbers.get()
    info_file_path = os.path.join(os.path.dirname(rules.get_accession_numbers.output[0]), '{accession}.txt')
    accessions = glob_wildcards(info_file_path).accession
    all_input['busco'] += expand(rules.busco_runs_single_assembly.output[0], accession=accessions)
    all_input['taxonomy'] = expand(rules.get_taxonomy.output[0], accession=accessions)
    all_input['info'] = expand(rules.extract_dataset.output.info, accession=accessions)
    return all_input


rule generate_summary_json:
    '''
    Generate a JSON summary of all metadata and results for all assemblies.
    '''
    input:
        unpack(summary_input)
    output:
        'results/summary.json'
    benchmark:
        'benchmarks/summary/generate_summary_json.tsv'
    log:
        'logs/summary/generate_summary_json.txt'
    conda:
        '../envs/summary.yaml'
    threads: get_threads('generate_summary_json')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('generate_summary_json', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('generate_summary_json', attempt)
    script:
        '../scripts/generate_summary_json.py'


rule generate_summary_table:
    '''
    Generate a table summary of all metadata and results for all assemblies.
    Fields to include in the table, as well as their labels and order, are
    defined in a JSON file specified in the config file.
    '''
    input:
        rules.generate_summary_json.output
    output:
        'results/summary.tsv'
    benchmark:
        'benchmarks/summary/generate_summary_table.tsv'
    log:
        'logs/summary/generate_summary_table.txt'
    conda:
        '../envs/summary.yaml'
    threads: get_threads('generate_summary_table')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('generate_summary_table', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('generate_summary_table', attempt)
    params:
        summary_fields_file = config['summary']['summary_fields_file']
    script:
        '../scripts/generate_summary_table.py'
