wildcard_constraints:
    accession = 'GC[AF]_[0-9]{9}.[0-9]{1,3}',  # Strict constraint on accession number
    lineage = '[a-z]+'


def flatten(iterable):
    '''
    Flattens an iterable of iterables into a iterable of single values, e.g.:
    [[1, 2], [3], 4] --> [1, 2, 3, 4]
    '''
    for element in iterable:
        if isinstance(element, collections.abc.Iterable) and not isinstance(element, (str, bytes)):
            yield from flatten(element)
        else:
            yield element
