rule generate_run_info:
    '''
    '''
    input:
        ncbi_query_flag_file = rules.get_accession_numbers.output,
        summary_file = rules.generate_summary_table.output
    output:
        'results/run_info.txt'
    benchmark:
        'benchmarks/generate_run_info.tsv'
    log:
        'logs/generate_run_info.txt'
    conda:
        '../envs/summary.yaml'
    threads: get_threads('generate_run_info')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('generate_run_info', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('generate_run_info', attempt)
    params:
        runs_dir = 'results/runs'
    script:
        '../scripts/generate_run_info.py'


rule save_run_results:
    '''
    '''
    input:
        json_summary = rules.generate_summary_json.output,
        table_output = rules.generate_summary_table.output,
        web_output = rules.generate_summary_html.output,
        run_info = rules.generate_run_info.output
    output:
        directory('results/runs/{run_id}')
    benchmark:
        'benchmarks/save_run_results/{run_id}.tsv'
    log:
        'logs/save_run_results/{run_id}.txt'
    threads: get_threads('save_run_results')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('save_run_results', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('save_run_results', attempt)
    params:
        web_output_dir = lambda wildcards, input: os.path.dirname(input.web_output[0])
    shell:
        'mkdir {output} 2> {log}; '
        'cp -rf {params.web_output_dir} {output} 2>> {log};'
        'cp -f {input.json_summary} {output} 2>> {log};'
        'cp -f {input.table_output} {output} 2>> {log};'
        'cp -f {input.run_info} {output} 2>> {log}'


rule update:
    '''
    '''
    input:
        rules.get_accession_numbers.output[0],
        rules.generate_summary_json.output[0],
        rules.generate_summary_table.output[0],
        rules.setup_web_folder.output,
        rules.generate_summary_html.output[0]
    log:
        'logs/update.txt'
    threads: get_threads('update')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('update', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('update', attempt)
    shell:
        'rm -rf {input} 2> {log}'
