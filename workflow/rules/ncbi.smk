
rule download_ete3_db:
    '''
    Download NCBI taxonomy information for ete3 phylogeny functions.
    '''
    output:
        touch('results/.etetoolkit/.done')
    benchmark:
        'benchmarks/download_ete3_db.tsv'
    log:
        'logs/download_ete3_db.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('download_ete3_db')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('download_ete3_db', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('download_ete3_db', attempt)
    params:
        db_path = lambda wildcards, output: os.path.join(os.path.dirname(output[0]), 'taxa.sqlite')
    script:
        '../scripts/download_ete3_db.py'


# Set a variable for ete3 database path
ETE3_DB_PATH = os.path.join(os.path.dirname(rules.download_ete3_db.output[0]), 'taxa.sqlite')


checkpoint get_accession_numbers:
    '''
    Query NCBI datasets to retrieve accession numbers for all assemblies in
    base_taxon. Accession numbers are stored as:
    'results/.accession_numbers/{accession}.txt'
    When updating the workflow, only adds new accession numbers since last run
    to preserve Snakemake timestamp-based dependency system.
    '''
    output:
        touch('results/.accession_numbers/.done')
    benchmark:
        'benchmarks/get_accession_numbers.tsv'
    log:
        'logs/get_accession_numbers.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('get_accession_numbers')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('get_accession_numbers', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('get_accession_numbers', attempt)
    params:
        base_taxon = config['base_taxon']
    script:
        '../scripts/get_accession_numbers.py'


rule download_dataset:
    '''
    '''
    input:
        os.path.join(os.path.dirname(rules.get_accession_numbers.output[0]), '{accession}.txt')
    output:
        temp('results/.downloads/tmp_{accession}.zip')
    benchmark:
        'benchmarks/download_dataset/{accession}.tsv'
    log:
        'logs/download_dataset/{accession}.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('download_dataset')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('download_dataset', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('download_dataset', attempt)
    params:
        datasets_path = config['ncbi_datasets_path']
    shell:
        '{params.datasets_path} '
        'download '
        'genome '
        'accession {wildcards.accession} '
        '--exclude-gff3 '
        '--exclude-protein '
        '--exclude-rna '
        '--filename {output} '
        '2>&1 > {log}'


rule extract_dataset:
    '''
    Download a dataset from NCBI using the dataset API and save the assembly
    as fasta and the metadata as json.
    '''
    input:
        rules.download_dataset.output[0]
    output:
        assembly = 'results/assemblies/{accession}.fna',
        info = 'results/info/{accession}.json',
    benchmark:
        'benchmarks/extract_dataset/{accession}.tsv'
    log:
        'logs/extract_dataset/{accession}.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('extract_dataset')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('extract_dataset', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('extract_dataset', attempt)
    script:
        '../scripts/extract_dataset.py'


rule get_taxonomy:
    '''
    Retrieve taxonomy information for an assembly using ete3.
    '''
    input:
        rules.download_ete3_db.output,
        info_file = rules.extract_dataset.output.info
    output:
        'results/taxonomy/{accession}.json'
    benchmark:
        'benchmarks/get_taxonomy/{accession}.tsv'
    log:
        'logs/get_taxonomy/{accession}.txt'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('get_taxonomy')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('get_taxonomy', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('get_taxonomy', attempt)
    params:
        ete3_db = ETE3_DB_PATH
    script:
        '../scripts/get_taxonomy.py'
