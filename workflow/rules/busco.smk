import os

checkpoint determine_busco_runs:
    '''
    Retrieve all available lineage datasets from BUSCO's website and cross them
    with taxonomy information to determine all relevant lineages for an assembly.
    '''
    input:
        taxonomy = rules.get_taxonomy.output,
        info = rules.extract_dataset.output.info,
        blacklist = config['busco_runs_blacklist_file']
    output:
        directory('results/.busco_runs/{accession}')
    log:
        'logs/determine_busco_runs/{accession}.log'
    benchmark:
        'benchmarks/determine_busco_runs/{accession}.tsv'
    conda:
        '../envs/ncbi.yaml'
    threads: get_threads('determine_busco_runs')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('determine_busco_runs', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('determine_busco_runs', attempt)
    params:
        dataset_base_url = config['busco_dataset_base_url'],
        min_assembly_size = config['busco']['min_assembly_size'],
        min_assembly_n50 = config['busco']['min_assembly_n50']
    script:
        '../scripts/determine_busco_runs.py'


rule busco_run:
    '''
    Run BUSCO v5 on a single assembly with a single lineage.
    The entire results folder is stored as an archive except for blast_db and
    the redundant genome sequence in blast_output; only directly relevant
    files are retained as plain files.
    '''
    input:
        assembly = rules.extract_dataset.output.assembly
    output:
        tarball = 'results/busco/{accession}/{lineage}/{accession}_{lineage}.tar.gz',
        table = 'results/busco/{accession}/{lineage}/full_table.tsv',
        missing_busco = 'results/busco/{accession}/{lineage}/missing_busco_list.tsv',
        summary = 'results/busco/{accession}/{lineage}/short_summary.txt',
        summary_json = 'results/busco/{accession}/{lineage}/short_summary.json'
    log:
        'logs/busco/{accession}/{lineage}.log'
    benchmark:
        'benchmarks/busco/{accession}/{lineage}.tsv'
    conda:
        '../envs/busco.yaml'
    shadow: 'minimal'
    threads: get_threads('busco_run')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('busco_run', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('busco_run', attempt)
    params:
        mode = config['busco']['mode'],
        extra = config['busco']['extra'] if config['busco']['extra'] else '',
        output_root = lambda wildcards: f'results/busco/{wildcards.accession}',
        output_dir = lambda wildcards: f'{wildcards.accession}_{wildcards.lineage}',
        final_dir = lambda wildcards: f'results/busco/{wildcards.accession}/{wildcards.lineage}',
        tmp_tarball = lambda wildcards: f'tmp_{wildcards.accession}_{wildcards.lineage}.tar.gz'
    shell:
        'busco --force '
        '--in {input.assembly} '
        '--lineage_dataset {wildcards.lineage} '
        '--out_path {params.output_root} '
        '--out {params.output_dir} '
        '--mode {params.mode} '
        '--cpu {threads} '
        '{params.extra} '
        '> {log} 2>&1;'
        'mv {params.output_root}/{params.output_dir}/* {params.final_dir} >> {log} 2>&1;'
        'rm -rf {params.output_root}/{params.output_dir} >> {log} 2>&1;'
        'tar cvf - -C results/busco {wildcards.accession}/{wildcards.lineage} 2>> {log} | '
        'xz -T {threads} -zkf -9 - > {params.tmp_tarball} 2>> {log};'
        'mv {params.tmp_tarball} {output.tarball} 2>> {log};'
        'rm -rf {params.final_dir}/logs 2>> {log};'
        'rm -rf {params.final_dir}/prodigal_output 2>> {log};'
        'rm -f {params.final_dir}/short_summary*.txt 2>> {log};'
        'rm -f {params.final_dir}/short_summary*.json 2>> {log};'
        'mv -f {params.final_dir}/run_*/*.{{txt,tsv,json}} {params.final_dir} 2>> {log};'
        'rm -rf {params.final_dir}/run_* 2>> {log};'


def busco_runs_single_assembly_input(wildcards):
    '''
    '''
    checkpoints.determine_busco_runs.get(accession=wildcards.accession)
    lineage_file_path = os.path.join(rules.determine_busco_runs.output[0].format(accession=wildcards.accession), '{lineage}.txt')
    lineages = glob_wildcards(lineage_file_path).lineage
    return expand(rules.busco_run.output.summary, accession=wildcards.accession, lineage=lineages)


rule busco_runs_single_assembly:
    '''
    Aggregator rule. Redundant but it's required to properly parallelize
    steps in the workflow, without it datasets are downloaded one by one.
    '''
    input:
        busco_runs_single_assembly_input
    output:
        touch('results/busco/{accession}/.done')
    log:
        'logs/busco_runs_single_assembly/{accession}.log'
    benchmark:
        'benchmarks/busco_runs_single_assembly/{accession}.tsv'
    threads: get_threads('busco_runs_single_assembly')
    resources:
        mem_mb = lambda wildcards, attempt: get_mem('busco_runs_single_assembly', attempt),
        runtime_s = lambda wildcards, attempt: get_runtime('busco_runs_single_assembly', attempt)
